# Contributing

Thank you for reading this, either you are curious or you want to help us improving this project.

Before helping us, please be aware we are following the contributor covenant code of conduct and expect you to do so as well.

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](https://www.contributor-covenant.org/version/2/0/code_of_conduct/)

## What can you do to help

We choose to rebase each step so the git tree remains minimal and we intend to keep it that way.
This means that we won't accept merge request from anyone.

This being said, for any suggestion or remark, please feel free to use the issues to get in touch with us.
Every help will be appreciated (spell-check, diagrams, mistakes, bug, extensions, etc.)
