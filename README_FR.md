# Kebab Dojo

*Available also in [English](readme.md)*

---

## Objectifs

Ce DOJO vous propose un exemple d'implémentation d'un projet en dotnet core en utilisant des techniques et patterns issues des concepts suivants :

- DDD – Domain Driven Design
- TDD – Test Driven Development
- CQS – Command Query Separation
- Architecture hexagonale

Il est constitué d'une présentation de théorie ainsi que d'une suite d'étapes, permettant de mettre en application les principes **DDD**, **TDD**, **CQS** et d'**Architecture hexagonale**.

La présentation a un double objectif :

1. Se familiariser avec les concepts appliqusé dans le DOJO
2. Offrir un support d'introduction pour l'organisateur du DOJO

Le DOJO en lui même est constitué de 13 étapes successives de développement d'une application en dotnet core.

## Cible et prérequis

Ce DOJO s'adresse aux développeurs C#.

Pour pouvoir réaliser ce DOJO, vous aurez besoin:

- d'un IDE (de préférence Visual Studio ou Visual Studio Code)
- dotnet core à jour
- Git et un terminal bash (git bash)

## Fonctionnement

### Présentation

Le point d'entré du DOJO est la présentation. Elle contient les éléments essentiels pour comprendre le contenu du DOJO et la méthodologie de développement logiciel proposée. Elle contient également une liste de ressources pour approfondir les concepts abordés.

> **La simple lecture de la présentation et la réalisation du DOJO ne sont pas suffisante pour maitriser les concepts présentés.**

Cette présentation est au format "slides" pour permettre l'organisation en groupe de ce DOJO.

### DOJO

Le DOJO est constitué d'étapes successives présentées comme un jeu. A chaque étape, le développeur est amené à implémenter des fonctions pour faire passer les tests unitaires.

Commencer par cloner le repository (vous devriez arriver sur la branche master) puis lancer le script `next.sh`.

Ce script devra être lancé à chaque fois que vous penserez avoir complété l'étape pour pouvoir passer à la suivante.

Un second script vous permet de relire les instructions de l'étape en cours: `instruction.sh`  

Pour recommencer depuis le début: `reset.sh`

En arrivant à chaque étape:

1. Lisez le contenu de instruction.sh (normalement affiché par le script `next.sh`)
2. Executez les tests de la solution
3. Implémentez les méthodes générant les échecs des tests
4. Lancez le script `next.sh` pour valider votre implémentation et passer à l'étape suivante

Quelques règles:

- Il n'est pas necessaire de créer de nouveaux fichiers (ceux ci seront supprimés au passage vers l'étape suivante)
- Il n'est pas nécessaire d'écrire du code en dehors des méthodes faisant échouer les tests.
- Votre code est suprimé lors d'un passage d'une étape à l'autre, si vous souhaitez le garder pour faire la différence avec notre implémentation => faites un commit, tagger le pour le retrouver facilement (hint: step-{step number}-answer)

### Et après...

Nous avons fait ce DOJO car nous croyons dans le partage d'informations et en ces pratiques de développement. Pour propager cet esprit nous vous encourageons à continuer à lire sur ces sujets et parler avec vos collègues de ces pratiques de développement.

Nous n'avons pas la vérité absolue et si vous trouvez que notre contenu peut être amélioré, postez une "issue" sur notre gitlab avec vos propositions, nous regarderons ensemble comment améliorer le DOJO.

## Contexte et déroulé

Un vendeur de Kebab veut améliorer la qualité de son restaurant en proposant une expérience de commande informatisé via une application. Sur cette application, les clients peuvent composer leur Kebab et les envoyer en cuisine.

Vous êtes développeur et vous implémentez le backend de la solution.

### Étape 1

#### Description

Un utilisateur peut ajouter des ingredients à un kebab. Cette étape consiste en l'implémentation de cette fonctionnalité.

#### Technique

DDD : Implémentation d'un Aggregat / Entité composé de Value Object

TDD :

- Usage du pattern NotImplementedException
- Découverte du SUT (System Under Test, cf [Mark Seemann](https://blog.ploeh.dk/) )
- Nomage des méthodes de test
- Usage des commentaires Act / Arrange / Assert ([Roy Osherove](https://osherove.com/))

### Étape 2

#### Description

En fonction des ingrédients du kebab, ce dernier est végétarien ou non. Cette étape consiste en l'implémentation de cette différenciation.

### Étape 3

#### Description

Un kebab ne peut pas contenir plus de 5 ingrédients (par manque de place et pour limiter les clients). Cette étape consiste en l'implémentation de cette règle métier.

### Étape 4

#### Description

Pour passer commande du kebab et le placer en cuisine, il faut que le kebab contienne au moins 1 ingrédient. Cette étape consiste en l'implémentation de cette vérification.

### Étape 5

#### Description

Une fois envoyé en cuisine le client ne peut plus modifier un kebab. Cette étape implémente cette vérification.

#### Technique

TDD : Utilisation du [pattern builder](https://blog.ploeh.dk/2017/08/15/test-data-builders-in-c/) pour la création d'objets complexes dans une méthode de test.

### Étape 6

#### Description

Cette étape consiste en l'implémentation de l'émission de l'événement du domaine associé à l'envoi d'un kebab en cuisine.

#### Technique

DDD : L'expression de changements dans le domaine passe par le développement de DomainEvent.

### Étape 7

#### Description

En architecture hexagonal, la couche applicative est considérée comme un adapteur. C'est cet adapteur qui contient les cas d'utilisation de l'application.

CQS définit deux types d'intéractions applicatives : les "commands" et les "queries".

Cette étape implémente la "command" de création d'un kebab, prêt à être envoyé en cuisine !

#### Technique

DDD : Utilisation d'une factory pour l'instanciation d'aggrégat

CQS :

- Séparation des "commands" et des "queries" au niveau du code
- Execution d'une "command" dans le cadre d'une transaction applicative

### Étape 8

#### Description

Cette étape implémente la query de consultation des données d'un kebab.

#### Technique

DDD : Utilisation d'un repository pour la récupération d'aggrégat

### Étape 9

#### Description

Pour permettre aux adapteurs intéressés par les changements au sein du domaine (menant à la création de "domain events"), il propose un SPI IDomainEventHandler. Lors de la validation de la transaction applicative (commit), tous les événements du domaine doivent être executés par ces "handlers".

Cette étape implémente la validation de la transaction applicative et le conteneur d'événements.

### Étape 10

#### Description

La persistence des données est elle aussi concidérée comme un adapteur en architecture hexagonnale.
Cette étape implémente le Repository de l'aggrégat.

#### Technique

TDD : Utilisation du InMemoryContext de EF

Architecture Hexagonnale : Implémentation de SPI

### Étape 11

#### Description

La sauvegarde en base de données du kebab fait également partie de l'adapteur "persistence".

Cette étape implémente le "handler" de l'événement du domaine associé à la création d'un kebab.

#### Technique

Architecture Hexagonnale : Séparation stricte du métier et de la persistance.

### Étape 12

#### Description

Si une autre partie de l'application veut réagir à un événement du domaine, il "suffit" de rajouter le "handler" correspondant.

Cette étape implémente un deuxième "handler" associé à l'événement de création d'un kebab.

### Étape 13

Dernière étape, pas de code à produire mais deux tests d'exemple permettant de prouver que le système fonctionne dans son intégralité.
