#!/usr/bin/env bash

BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
BROWN='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT_GRAY='\033[0;37m'
NC='\033[0m'

if [ $# -eq 0 ]
  then
    echo "No arguments supplied, dafaulting to branch name..."
    BRANCH_NAME=`git rev-parse --abbrev-ref HEAD`
else
    BRANCH_NAME=$1
fi

PREFIX=`cat .dojolang`-
STEP_NAME=`echo ${BRANCH_NAME} | sed -e "s/${PREFIX}//"`

echo "Displaying instruction for ${STEP_NAME}"
case "${STEP_NAME}" in
"step-1")
    echo -e "${BROWN}Welcome to this first step of the dojo.${NC}"
    echo -e "${BLUE}Instruction for step 1:${NC}"
    echo -e "We implemented:"
    echo -e "  - The `cat .dojolang` solution holding our codebase"
    echo -e "  - A lib containing our production code"
    echo -e "  - All tests for the KebabService project"
    echo -e "  - A value object Ingredient representing ingredient of our kebab"
    echo -e "  - The root aggregate of our solution: Kebab"
    echo -e "  - The test for adding an ingredient to the kebab"
    echo -e "${GREEN}> Please implement adding and ingredient to a kebab${NC}"
    ;;
"step-2")
    echo -e "${BLUE}Instruction for step 2:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - The test for controlling our kebab is veggie"
    echo -e "${GREEN}> Please implement the veggie verification${NC}"
    ;;
"step-3")
    echo -e "${BLUE}Instruction for step 3:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - A root exception for all domain exceptions: DomainException"
    echo -e "  - The TooManyIngredientsException"
    echo -e "  - An updated version of the test for adding an ingredient"
    echo -e "${GREEN}> Please implement the ingredient limit in kebab${NC}"
    ;;
"step-4")
    echo -e "${BLUE}Instruction for step 4:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - The NoIngredientInKebabException"
    echo -e "  - The tests describing the behaviour for placing a kebab"
    echo -e "${GREEN}> Please implement the placing of a kebab${NC}"
    ;;
"step-5")
    echo -e "${BLUE}Instruction for step 5:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - The CannotAddIngredientOnPlacedKebabException and the CannotPlacePlacedKebabException"
    echo -e "  - The KebabBuilder in the testing project"
    echo -e "  - An updated version of the existing tests to meet project requirements"
    echo -e "${GREEN}> Please implements the verification locking the kebab after it is placed${NC}"
    ;;
"step-6")
    echo -e "${BLUE}Instruction for step 6:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - A root event for all domain events"
    echo -e "  - An interface for storing our domain events"
    echo -e "  - The Kebab aggregate have now access to that storage"
    echo -e "  - We defined the KebabPlacedEvent"
    echo -e "  - We updated the tests accordingly"
    echo -e "${GREEN}> Please implement the expression of domain changes with the KebabPlacedEvent${NC}"
    ;;
"step-7")
    echo -e "${BLUE}Instruction for step 7:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - An implementation for IDs generation"
    echo -e "  - An implementation of KebabFactory and its tests KebabFactoryTest"
    echo -e "  - The skeleton of PlaceKebabCommandHandler"
    echo -e "  - A helper class to help mapping between the application layer and the domain: DomainIngredientMapping"
    echo -e "  - A root exception for all application exceptions: ApplicationException"
    echo -e "  - The UnsupportedIngredientTypeException"
    echo -e "  - The interface IApplicationTransaction"
    echo -e "  - The tests for the PlaceKebabCommandHandler"
    echo -e "${GREEN}> Please implement the application use case to create and place kebab${NC}"
    ;;
"step-8")
    echo -e "${BLUE}Instruction for step 8:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - The interface of a kebab repository"
    echo -e "  - A new version of DomainIngredientMapping"
    echo -e "  - The skeleton of GetKebabQueryHandler"
    echo -e "  - The KebabNotFoundException"
    echo -e "  - The tests for GetKebabQueryHandler"
    echo -e "${GREEN}> Please implement th application use case to retreive a kebab${NC}"
    ;;
"step-9")
    echo -e "${BLUE}Instruction for step 9:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - An empty DomainEventContainer"
    echo -e "  - An empty ApplicationTransaction"
    echo -e "  - The interface for handling domain events"
    echo -e "  - The tests of DomainEventContainer and ApplicationTransaction"
    echo -e "${GREEN}> Please implement the handling of domain events with an application transaction${NC}"
    ;;
"step-10")
    echo -e "${BLUE}Instruction for step 10:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - A new Kebab constructor"
    echo -e "  - A helper class to help mapping between the persistence layer and the domain: DomainIngredientMapping"
    echo -e "  - A class to store our kebab in the database: KebabEntity"
    echo -e "  - We use an abstraction layer to access and store our data"
    echo -e "  - An empty KebabRepository"
    echo -e "  - The tests of KebabRepository"
    echo -e "${GREEN}> Please implement the repository giving access to kebabs${NC}"
    ;;
"step-11")
    echo -e "${BLUE}Instruction for step 11:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - The interface IUnitOfWork implemented by KebabDbContext"
    echo -e "  - An empty KebabPlacedEventHandler in the persistence layer"
    echo -e "  - The tests of KebabPlacedEventHandler"
    echo -e "  - The updated tests of ApplicationTransaction"
    echo -e "${GREEN}> Please implement storing the kebab based on the domain event${NC}"
    ;;
"step-12")
    echo -e "${BLUE}Instruction for step 12:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - The interface UnitOfWork"
    echo -e "  - An empty KebabPlacedEventHandler in the messaging layer"
    echo -e "  - The interface SendMessageService"
    echo -e "  - The tests of the new KebabPlacedEventHandler"
    echo -e "${GREEN}> Please implement the send of an integration message based on domain event${NC}"
    ;;
"step-13")
    echo -e "${BROWN}Final step of the dojo... You made it :)${NC}"
    echo -e "${BLUE}Instruction for step 13:${NC}"
    echo -e "We implemented:"
    echo -e "  - Our implementation of the previous step"
    echo -e "  - A minimal version of what IOC could be"
    echo -e "  - Two failing component tests for our two use cases: PlaceKebabCommandHandler and GetKebabQueryHandler"
    echo -e "${GREEN}> Please take a look at those two tests${NC}"
    echo -e "${BROWN}Don't hesitate to share with us your thoughs about this DOJO on our gitlab page :)${NC}"
    ;;
*)
    echo -e "${RED}Unknown step please run ${0} step-N (with N being in range [1, 13])${NC}"
    exit 1
    ;;
esac
